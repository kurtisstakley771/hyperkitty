# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-20 11:23-0800\n"
"PO-Revision-Date: 2021-05-19 11:33+0000\n"
"Last-Translator: Lens0021 <lorentz0021@gmail.com>\n"
"Language-Team: Korean <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/ko/>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.7-dev\n"

#: forms.py:53
msgid "Add a tag..."
msgstr ""

#: forms.py:55
msgid "Add"
msgstr ""

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr ""

#: forms.py:64
msgid "Attach a file"
msgstr "파일 첨부하기"

#: forms.py:65
msgid "Attach another file"
msgstr "다른 파일 첨부하기"

#: forms.py:66
msgid "Remove this file"
msgstr "파일 제거하기"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "에러 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "이런!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "페이지를 찾을 수 없습니다."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "홈으로 돌아가기"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "에러 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr "죄송합니다, 서버 오류로 인해 요청한 페이지를 불러올 수 없습니다."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "started"
msgstr "시작함"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "최근 활동:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "스레드 보기"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(추천 없음)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "방금 보냈지만 아직 처리되지 않았습니다"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "REST API"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"허이퍼키티는 당신이 프로그램을 이용해서 이메일과 정보를 불러올 수 있도록 간단"
"한 REST API를 함께 제공합니다."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "포맷"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"REST API는 여러 포맷으로 정보를 불러올 수 있습니다.  기본 포맷은 사람이 읽을 "
"수 있는 html 형식입니다."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"포맷을 변경하고 싶으면, <em>?format=&lt;FORMAT&gt;</em>을 URL에 추가하세요."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "다음은 이용 가능한 포맷입니다:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "일반 텍스트"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "메일 목록"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "종단점 :"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr "이 주소를 이용하시면 메일 목록에 대한 모든 정보를 받을 수 있습니다."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "메일 목록의 스레드"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"이 주소를 이용하시면 특정 메일 목록에 대한 모든 스레드를 불러오실수 있습니다."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "스레드에 있는 이메일"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"이 주소를 이용하시면 메일 작성 스레드의 이메일 주소를 불러올 수 있습니다."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "메일 작성 리스트의 이메일 주소"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"이 주소를 이용하시면 특정 메일 작성 리스트의 특정 이메일 주소의 정보에 대해"
"서 불러올수 있습니다."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "태그"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "이 주소를 이용하시면 태그 목록을 불러올수 있습니다."

#: templates/hyperkitty/base.html:57 templates/hyperkitty/base.html:112
msgid "Account"
msgstr "계정"

#: templates/hyperkitty/base.html:62 templates/hyperkitty/base.html:117
msgid "Mailman settings"
msgstr "Mailman 설정"

#: templates/hyperkitty/base.html:67 templates/hyperkitty/base.html:122
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "포스팅 기록"

#: templates/hyperkitty/base.html:72 templates/hyperkitty/base.html:127
msgid "Logout"
msgstr "로그아웃"

#: templates/hyperkitty/base.html:78 templates/hyperkitty/base.html:134
msgid "Sign In"
msgstr "로그인"

#: templates/hyperkitty/base.html:82 templates/hyperkitty/base.html:138
msgid "Sign Up"
msgstr "회원가입"

#: templates/hyperkitty/base.html:91
msgid "Search this list"
msgstr "이 목록에서 검색하기"

#: templates/hyperkitty/base.html:91
msgid "Search all lists"
msgstr "모든 목록에서 검색하기"

#: templates/hyperkitty/base.html:149
msgid "Manage this list"
msgstr "이 목록 관리하기"

#: templates/hyperkitty/base.html:154
msgid "Manage lists"
msgstr "목록 관리하기"

#: templates/hyperkitty/base.html:192
msgid "Keyboard Shortcuts"
msgstr "키보드 단축키"

#: templates/hyperkitty/base.html:195
msgid "Thread View"
msgstr "스레드 보기"

#: templates/hyperkitty/base.html:197
msgid "Next unread message"
msgstr "다음 안 읽은 메시지"

#: templates/hyperkitty/base.html:198
msgid "Previous unread message"
msgstr "이전 안 읽은 메시지"

#: templates/hyperkitty/base.html:199
msgid "Jump to all threads"
msgstr "모든 스레드로 이동"

#: templates/hyperkitty/base.html:200
msgid "Jump to MailingList overview"
msgstr "목록 개요로 이동"

#: templates/hyperkitty/base.html:214
#, fuzzy
msgid "Powered by"
msgstr "구동"

#: templates/hyperkitty/base.html:214
msgid "version"
msgstr "버전"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "아직 실행되지 않았습니다"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "실행되지 않음"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "이 기능은 아직 구현되지 않았습니다, 미안."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "에러 : 전용 목록"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"이 메일 작성 목록은 특정 전용입니다. 이 항목을 보고싶다면 가입해야합니다."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "좋아요 (취소)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "싫어요 (취소)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "투표하기 위해서 로그인을 해야합니다."

#: templates/hyperkitty/fragments/month_list.html:6
msgid "Threads by"
msgstr "스레드 구분:"

#: templates/hyperkitty/fragments/month_list.html:6
msgid " month"
msgstr " 작성 월"

#: templates/hyperkitty/fragments/overview_threads.html:11
msgid "New messages in this thread"
msgstr "스레드에 새 메세지가 있습니다"

#: templates/hyperkitty/fragments/overview_threads.html:36
#: templates/hyperkitty/fragments/thread_left_nav.html:19
#: templates/hyperkitty/overview.html:78
msgid "All Threads"
msgstr "모든 스레드"

#: templates/hyperkitty/fragments/overview_top_posters.html:15
msgid "See the profile"
msgstr "프로필 보기"

#: templates/hyperkitty/fragments/overview_top_posters.html:21
msgid "posts"
msgstr "게시글"

#: templates/hyperkitty/fragments/overview_top_posters.html:26
msgid "No posters this month (yet)."
msgstr "이번달에 작성된 게시글이 없습니다 (아직)."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "이 메세지는 다음과 같이 전송될 것입니다:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "발신인 바꾸기"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "다른 주소 링크하기"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr "현재 목록 멤버가 아닌 경우 이 메시지를 보내면 구독됩니다."

#: templates/hyperkitty/fragments/thread_left_nav.html:12
msgid "List overview"
msgstr "목록 개요"

#: templates/hyperkitty/fragments/thread_left_nav.html:29 views/message.py:74
#: views/mlist.py:102 views/thread.py:168
msgid "Download"
msgstr "다운로드"

#: templates/hyperkitty/fragments/thread_left_nav.html:32
msgid "Past 30 days"
msgstr "30일 전"

#: templates/hyperkitty/fragments/thread_left_nav.html:33
msgid "This month"
msgstr "이번 달"

#: templates/hyperkitty/fragments/thread_left_nav.html:36
msgid "Entire archive"
msgstr "모든 저장소"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "이용가능한 목록"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "인기순"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "최근 참가자들의 수로 정렬하기"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "활발한순"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "댓글순"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "이름순"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "가나다순"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "최근 생성순"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "생성 날짜순"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "로 정렬하기"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "비활성화 숨기기"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "특정 전용 숨기기"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "목록 찾기"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:191
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "new"

#: templates/hyperkitty/index.html:135 templates/hyperkitty/index.html:202
msgid "private"
msgstr "특정 전용"

#: templates/hyperkitty/index.html:137 templates/hyperkitty/index.html:204
msgid "inactive"
msgstr "비활성"

#: templates/hyperkitty/index.html:143 templates/hyperkitty/index.html:229
#: templates/hyperkitty/overview.html:94 templates/hyperkitty/overview.html:111
#: templates/hyperkitty/overview.html:181
#: templates/hyperkitty/overview.html:188
#: templates/hyperkitty/overview.html:195
#: templates/hyperkitty/overview.html:204
#: templates/hyperkitty/overview.html:212 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "로딩중..."

#: templates/hyperkitty/index.html:160 templates/hyperkitty/index.html:237
msgid "No archived list yet."
msgstr "보관된 목록이 아직 없습니다."

#: templates/hyperkitty/index.html:172
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "목록"

#: templates/hyperkitty/index.html:173
msgid "Description"
msgstr "설명"

#: templates/hyperkitty/index.html:174
msgid "Activity in the past 30 days"
msgstr "지난 30일간 활동 목록"

#: templates/hyperkitty/index.html:218 templates/hyperkitty/overview.html:103
#: templates/hyperkitty/thread_list.html:40
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:54
msgid "participants"
msgstr "참여자"

#: templates/hyperkitty/index.html:223 templates/hyperkitty/overview.html:104
#: templates/hyperkitty/thread_list.html:45
msgid "discussions"
msgstr "댓글"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "메일링 목록 삭제"

#: templates/hyperkitty/list_delete.html:20
msgid "Delete Mailing List"
msgstr "메일링 목록 삭제"

#: templates/hyperkitty/list_delete.html:26
msgid ""
"will be deleted along with all the threads and messages. Do you want to "
"continue?"
msgstr "모든 스레드 및 메시지와 함께 삭제됩니다. 계속하시겠습니까?"

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
msgid "Delete"
msgstr "삭제"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "또는"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "취소"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "스레드"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "삭제된 메세지"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s개의 메세지가 삭제될겁니다. 확실하십니까?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "새로운 스레드 생성하기"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "에"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "보내기"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "%(name)s 프로필 보기"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "읽지 않음"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "보낸 사람의 시각:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "새 주제:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "첨부파일:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "고정된 폰트로 보기"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "메세지에 고정된 하이퍼링크 달기"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "답장"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "온라인으로 답장하기위해 로그인하기"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s 첨부 파일\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "인용"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "새로운 스레드 만들기"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "이메일 소프트웨어 사용하기"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "스레드로 돌아가기"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "목록으로 돌아가기"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "메세지 삭제하기"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                작성자 %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:38
msgid "Home"
msgstr "홈"

#: templates/hyperkitty/overview.html:41 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "통계"

#: templates/hyperkitty/overview.html:44
msgid "Threads"
msgstr "스레드"

#: templates/hyperkitty/overview.html:50 templates/hyperkitty/overview.html:61
#: templates/hyperkitty/thread_list.html:48
msgid "You must be logged-in to create a thread."
msgstr "스레드를 만들기 위해서 로그인되어야 합니다."

#: templates/hyperkitty/overview.html:63
#: templates/hyperkitty/thread_list.html:52
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">새 스레드를 시작</span><span class=\"d-md-"
"none\">(N)</span>"

#: templates/hyperkitty/overview.html:75
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"<span class=\"d-none d-md-inline\">구독 관리</span><span class=\"d-md-none"
"\">(S)</span>"

#: templates/hyperkitty/overview.html:81
#, fuzzy
#| msgid "Entire archive"
msgid "Delete Archive"
msgstr "모든 저장소"

#: templates/hyperkitty/overview.html:91
msgid "Activity Summary"
msgstr "활동내역"

#: templates/hyperkitty/overview.html:93
msgid "Post volume over the past <strong>30</strong> days."
msgstr "지난 <strong>30</strong>일 간 쓴 글."

#: templates/hyperkitty/overview.html:98
msgid "The following statistics are from"
msgstr "이 통계의 출처는"

#: templates/hyperkitty/overview.html:99
msgid "In"
msgstr "에"

#: templates/hyperkitty/overview.html:100
msgid "the past <strong>30</strong> days:"
msgstr "지난 <strong>30</strong>일 :"

#: templates/hyperkitty/overview.html:109
msgid "Most active posters"
msgstr "가장 활발한 글쓴이"

#: templates/hyperkitty/overview.html:118
msgid "Prominent posters"
msgstr "유명한 글쓴이"

#: templates/hyperkitty/overview.html:133
msgid "kudos"
msgstr "명성"

#: templates/hyperkitty/overview.html:152
msgid "Recent"
msgstr ""

#: templates/hyperkitty/overview.html:156
#, fuzzy
#| msgid "Most active"
msgid "Most Active"
msgstr "활발한순"

#: templates/hyperkitty/overview.html:160
#, fuzzy
#| msgid "Most popular"
msgid "Most Popular"
msgstr "인기순"

#: templates/hyperkitty/overview.html:166
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "즐겨찾기"

#: templates/hyperkitty/overview.html:170
msgid "Posted"
msgstr ""

#: templates/hyperkitty/overview.html:179
msgid "Recently active discussions"
msgstr "최근에 활동한 의논들"

#: templates/hyperkitty/overview.html:186
msgid "Most popular discussions"
msgstr "가장 유명한 의논들"

#: templates/hyperkitty/overview.html:193
msgid "Most active discussions"
msgstr "가장 활발한 의논들"

#: templates/hyperkitty/overview.html:200
msgid "Discussions You've Flagged"
msgstr "찜한 의논들"

#: templates/hyperkitty/overview.html:208
msgid "Discussions You've Posted to"
msgstr "게시한 의논들"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "스레드 다시 올리기"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "다른 스레드에 다시 올리기"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "다시 올릴 스레드:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "다시 올릴 곳:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "부모 스레드 찾기"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "찾기"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "이 스레드의 ID:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "실행"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(되돌리기 할 것이 없습니다!), 또는"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "스레드로 돌아가기"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "다음의 검색 결과입니다"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "검색 결과"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "검색 결과"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "질문 당"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "메세지"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "점수에 따라 정렬하기"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "최근순으로 정렬하기"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "오래된 순으로 정렬하기"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr "이 요청에 따른 이메일을 찾을 수 없습니다."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "요청이 비어있습니다."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "당신이 찾고자한 메세지가 아닙니다"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "이전"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "다음"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "첫 게시물"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "답장"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "스레드 별로 답장보기"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "날짜별로 답장보기"

#: templates/hyperkitty/thread_list.html:60
msgid "Sorry no email threads could be found"
msgstr "이메일 스레드를 찾을 수 없습니다"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "수정하기"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "수정하기 전에 로그인되어야 합니다."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "카테고리 없음"

#: templates/hyperkitty/threads/right_col.html:12
msgid "Age (days ago)"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:18
#, fuzzy
#| msgid "Last activity"
msgid "Last active (days ago)"
msgstr "최근 활동"

#: templates/hyperkitty/threads/right_col.html:40
#, python-format
msgid "%(num_comments)s comments"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:44
#, python-format
msgid "%(participants_count)s participants"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:49
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "즐겨찾기에 등록하기 위해서 로그인되어야 합니다."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "즐겨찾기에 추가하기"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "즐겨찾기에서 제거하기"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "스레드 다시 올리기"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "스레드 삭제하기"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "안읽음:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "이동:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "다음"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "이전"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "즐겨찾기"

#: templates/hyperkitty/threads/summary_thread_large.html:38
msgid "Most recent thread activity"
msgstr "가장 최근의 스레드 활동"

#: templates/hyperkitty/threads/summary_thread_large.html:59
msgid "comments"
msgstr "댓글"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "태그"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "태그 검색하기"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "제거"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "작성자"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "%(fullname)s의 프로필로 이동"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "이 유저의 이메일을 찾을 수 없습니다."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "사용자의 게시글 활동내역"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "-"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "읽은 스레드"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "투표"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "구독 목록"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "원작성자:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "시작됨:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "최근 활동:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "답장:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "주제"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "원작성자"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "시작한 날짜"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "최근 활동"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "즐겨찾기 없음."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "새 댓글"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "아무것도 안읽음."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "최근 게시글"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "날짜"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "스레드"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "최근 스레드 활동내역"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "작성한 글이 없습니다."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "첫 게시글부터"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "게시글"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "게시글 없음"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "첫 활동이후로 지난 시간"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "첫 게시글"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "이 목록의 게시글"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "가입한 목록 없음"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "당신이 좋아합니다"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "당신이 싫어합니다"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "투표"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "투표하지 않음."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "사용자 프로필"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "사용자 프로필"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "이름:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "창작품:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "이 사용자에게 투표하기:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "이메일 주소:"

#: views/message.py:75
msgid "This message in gzipped mbox format"
msgstr "이 메세지는 gzip mobx 포멧으로 압축되었습니다"

#: views/message.py:201
msgid "Your reply has been sent and is being processed."
msgstr "답장이 전송되어 처리 중입니다."

#: views/message.py:205
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  {} 목록을 아직 구독하지 않았습니다."

#: views/message.py:288
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "메세지를 삭제할 수 없습니다 %(msg_id_hash)s: %(error)s"

#: views/message.py:297
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "성공적으로 삭제했습니다 ( %(count)s 개)."

#: views/mlist.py:88
msgid "for this month"
msgstr "이번 달에"

#: views/mlist.py:91
msgid "for this day"
msgstr "하루 동안"

#: views/mlist.py:103
msgid "This month in gzipped mbox format"
msgstr "이번 달을 gzip mbox 포멧으로 압축된 형태"

#: views/mlist.py:200 views/mlist.py:224
msgid "No discussions this month (yet)."
msgstr "이번 달에 개시된 논의가 없습니다."

#: views/mlist.py:212
msgid "No vote has been cast this month (yet)."
msgstr "이번 달에 진행된 투표가 없습니다."

#: views/mlist.py:241
msgid "You have not flagged any discussions (yet)."
msgstr "아직 찜한 의논이 없습니다."

#: views/mlist.py:264
msgid "You have not posted to this list (yet)."
msgstr "이 목록에 아직 글을 쓰지 않았습니다."

#: views/mlist.py:357
msgid "You must be a staff member to delete a MailingList"
msgstr "메일링 목록을 삭제하려면 스태프 멤버여야 합니다"

#: views/mlist.py:371
msgid "Successfully deleted {}"
msgstr "성공적으로 삭제했습니다 {}"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "파싱(Parsing) 에러:%(error)s"

#: views/thread.py:169
msgid "This thread in gzipped mbox format"
msgstr "이 스레드는 gzip mbox 포멧으로 압축되었습니다"

#~ msgid "days inactive"
#~ msgstr "일 비활성화"

#~ msgid "days old"
#~ msgstr "일 지남"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                    by %(name)s\n"
#~ "                    "
#~ msgstr ""
#~ "\n"
#~ "                    작성자 %(name)s\n"
#~ "                    "

#~ msgid "unread"
#~ msgstr "안 읽은"

#~ msgid "Go to"
#~ msgstr "이동"

#~ msgid "More..."
#~ msgstr "더 보기..."

#~ msgid "Discussions"
#~ msgstr "의논"

#~ msgid "most recent"
#~ msgstr "가장 최근의"

#~ msgid "most popular"
#~ msgstr "가장 인기있는"

#~ msgid "most active"
#~ msgstr "가장 활발한"

#~ msgid "Update"
#~ msgstr "업데이트"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                                        by %(name)s\n"
#~ "                                    "
#~ msgstr ""
#~ "\n"
#~ "                                        작성자 %(name)s\n"
#~ "                                    "
